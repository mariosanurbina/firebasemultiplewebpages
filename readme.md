#Firebase Auth Multiple Web Pages

This repository gives a basic code to implement an Auth process in a project composed by multiple HTML files.
In the index.html file, there is a script called authCodeFirstPage.js. That script is the one in charge on signup and login functions. In case the Auth is successful it will redirect you to the web page you want the user land after have registered in this case otherPage.html.
In otherPage.html there is one script called routeProtection which it will ensure the user cannot see the this webpage if him has not been previosly authenticated, and also will contain the parameters of firebase and its initialization  which will mantain the consistency in the auth process between webpages.
With the property firebase.auth().currentUser we are going to be able to access properties of the customer like the email.
In each aditional web page  we will need to include this script. 
 
  


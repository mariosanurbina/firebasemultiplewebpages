 // Your web app's Firebase configuration
  
  const firebaseConfig = {
    apiKey: "//replace with your own information",
    authDomain: "//replace with your own information",
    databaseURL: "//replace with your own information",
    projectId:"//replace with your own information",
    storageBucket: "//replace with your own information",
    messagingSenderId: "//replace with your own information",
    appId: "//replace with your own information",
    measurementId: "//replace with your own information"
  };

  firebase.initializeApp(firebaseConfig);

 

  //Get elements
  const txtEmail = document.getElementById('txtEmail');
  const txtPassword = document.getElementById('txtPassword');
  const btnLogin = document.getElementById('btnLogin');
  const btnSignUp = document.getElementById('btnSignUp'); 


  //Add login event
  btnLogin.addEventListener('click', () =>{
    // Get email and password
    
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();
    
    //Sign in
    auth.signInWithEmailAndPassword(email,pass)
    .catch(errors => console.log(errors.message)); //errors
      
  });
  
  
  //Add sign up button
  btnSignUp.addEventListener('click',()=>{
    // Get email and password
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const auth = firebase.auth();

    //Sign in
   auth.createUserWithEmailAndPassword(email,pass)     
    .catch(errors => console.log(errors.message)); //errors
  });
  
  
  // Add real time listener
  firebase.auth().onAuthStateChanged(firebaseUser => {
    console.log(firebaseUser);
    if (firebaseUser){
      
      window.location.assign("otherPage.html");

      console.log(firebaseUser);  
      
      
    } else {
      
      console.log('This user has not been authtenticated');
      
    }
  });



 
 
 
